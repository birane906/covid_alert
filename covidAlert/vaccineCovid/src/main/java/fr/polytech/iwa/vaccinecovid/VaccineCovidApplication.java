package fr.polytech.iwa.vaccinecovid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccineCovidApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaccineCovidApplication.class, args);
	}

}
