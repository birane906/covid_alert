package fr.polytech.iwa.locationsuspicious;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationSuspiciousApplication {

    public static void main(String[] args) {
        SpringApplication.run(LocationSuspiciousApplication.class, args);
    }

}
