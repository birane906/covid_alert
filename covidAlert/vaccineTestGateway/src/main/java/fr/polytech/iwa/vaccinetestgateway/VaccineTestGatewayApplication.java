package fr.polytech.iwa.vaccinetestgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccineTestGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaccineTestGatewayApplication.class, args);
    }

}
