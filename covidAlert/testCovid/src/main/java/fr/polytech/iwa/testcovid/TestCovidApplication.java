package fr.polytech.iwa.testcovid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCovidApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCovidApplication.class, args);
    }

}
