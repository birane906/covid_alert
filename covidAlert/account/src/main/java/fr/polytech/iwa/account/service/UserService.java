package fr.polytech.iwa.account.service;

import fr.polytech.iwa.account.exception.UserNotFoundException;
import fr.polytech.iwa.account.model.User;
import fr.polytech.iwa.account.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(User user){
        return userRepository.save(user);
    }

    public User updateUser(User user){
        return userRepository.save(user);
    }



    public List<User> findAllUsers(){
        return userRepository.findAll();
    }

    public User findUserByUser_id(Long user_id) {
        return userRepository.findUserByUser_id(user_id).orElseThrow(()-> new UserNotFoundException("User by id" + user_id + " not found") );
    }
}

