cd ../account
docker build -t accountdockerimage .
docker run accountdockerimage &


cd ../authentication
docker build -t authenticationdockerimage .
docker run authenticationdockerimage &

cd ../location
docker build -t locationdockerimage .
docker run locationdockerimage &

cd ../locationSuspicious
docker build -t locationsuspiciousdockerimage .
docker run locationsuspiciousdockerimage &

cd ../mainGateway
docker build -t maingatewaydockerimage .
docker run maingatewaydockerimage &

cd ../testCovid
docker build -t testcoviddockerimage .
docker run testcoviddockerimage &

cd ../vaccineCovid
docker build -t vaccinecoviddockerimage .
docker run vaccinecoviddockerimage

cd ../vaccineTestGateway
docker build -t vaccinetestgatewaydockerimage .
docker run vaccinetestgatewaydockerimage
