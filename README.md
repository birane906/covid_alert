# CovidAlert

Spring Boot application made by BA Birane and BOURRET Raphaël in the course IWA2 from Polytech Montpellier France  

## Requirements

> Disclaimer : The project is in a development state and the requirements/run processes are not yet optimized for production 

- git
- docker
- IntelliJ IDEA

## Run

```sh
docker run -d -p 5432:5432 -v pgdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD="postgres" -e POSTGRES_USER="postgres" --name postgres-covid postgres:alpine
```

Run `covid_alert_db.sql` in Postgres to load the database

Change your current branch to one of the `dev-*`

Open `covidAlert/MICROSERVICE`, where `MICROSERVICE` matches your current branch, into IntelliJ IDEA

Run `bootRun` in the left panel under `Gradle`